<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property int $id ID
 * @property string $value value
 * @property string $title Name
 * @property string $created_date Start Date
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value', 'title', 'created_date'], 'required'],
            [['created_date'], 'safe'],
            [['value', 'title'], 'string', 'max' => 180],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
            'title' => 'Title',
            'created_date' => 'Created Date',
        ];
    }
}

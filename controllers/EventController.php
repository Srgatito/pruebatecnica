<?php

namespace app\controllers;

use Yii;
use app\models\Event;
use app\models\EventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionFilterEvents($choice = null) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $query = Events::find();

        if( is_null($choice) || $choice=='all'){
            //the function should return the same events that you were loading before
            $dbEvents = $query->all();

        } else{
            //here you need to look up into the data base
            //for the relevant events against your choice
            $dbEvents = $query->where(['=', 'column_name', ':choice'])
                ->params([':choice' => $choice])
                ->asArray()
                ->all();
        }

        return $this->loadEvents($dbEvents);
    }

    /**
     *
     * @param type $dbEvents
     * @return \yii2fullcalendar\models\Event
     */
    private function loadEvents($dbEvents) {
        foreach( $dbEvents AS $event ){
            //Testing
            $Event = new \yii2fullcalendar\models\Event();
            $Event->id = $event->id;
            $Event->title = $event->categoryAsString;
            $Event->start = date('Y-m-d\TH:i:s\Z', strtotime($event->date_start . ' ' . $event->time_start));
            $Event->end = date('Y-m-d\TH:i:s\Z', strtotime($event->date_end . ' ' . $event->time_end));
            $events[] = $Event;
        }
        return $events;
    }

    /**
     * Lists all Event models.
     * @return mixed
     */

    public function actionIndex()
    {
        $events = event::find()->all();
        $tasks = [];

        foreach ($events as $event){

            $event =  new \yii2fullcalendar\models\Event();
            $event->id = $event->id;
            $event->backgroundColor = 'green';
            $event->title = $event->title;
            $event->start = date('Y-m-d\Th:m:s\Z',strtotime('tomorrow 6am'));
            $tasks[] = $event;

        }

        return $this->render('index', [

            'events' => $tasks,]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Event();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
